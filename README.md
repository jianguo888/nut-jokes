# 基于ArkUI eTS开发的坚果笑话（NutJoke）

都说笑一笑十年少，确实，在生活中，我们也是很久没有笑了，那么今天，我就做一个鸿蒙eTS版的坚果笑话App，

## 实现的功能：

- 获取接口数据
- 笑话列表
- 笑话详情页

## 你能学到的有：

- 网络请求

- 可滚动组件

- 容器组件

- 路由跳转

- 基础组件

  



## 文件结构



```
.
├── config.json
├── ets
│   └── MainAbility
│       ├── app.ets
│       ├── common
│       │   └── RealtimeWeather.ets
│       ├── data
│       │   └── get_test.ets
│       ├── model
│       │   ├── jokeDetailModel.ets
│       │   └── jokeModel.ets
│       └── pages
│           ├── Main.ets
│           └── jokeDetails.ets
└── resources
    ├── base
    │   ├── element
    │   │   ├── color.json
    │   │   └── string.json
    │   └── media
    │       └── icon.png
    └── rawfile

```

## 效果预览：

![gif11](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/gif11.gif)

![gif10](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/gif10.gif)

![image-20220722101712051](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220722101712051.png)



## 笑话大全接口



我们用到的接口：

[聚合数据的笑话大全](https://www.juhe.cn/docs/api/id/95)

**接口地址：**http://v.juhe.cn/joke/content/list.php

**返回格式：**json

**请求方式：**http get

**请求示例：**http://v.juhe.cn/joke/content/list.php?key=您申请的KEY&page=2&pagesize=10&sort=asc&time=1418745237

**接口备注：**根据时间戳返回该时间点前或后的笑话列表

### 请求参数说明：



|      | 名称     | 必填 | 类型   | 说明                                                  |
| ---- | -------- | ---- | ------ | ----------------------------------------------------- |
|      | sort     | 是   | string | 类型，desc:指定时间之前发布的，asc:指定时间之后发布的 |
|      | page     | 否   | int    | 当前页数,默认1,最大20                                 |
|      | pagesize | 否   | int    | 每次返回条数,默认1,最大20                             |
|      | time     | 是   | string | 时间戳（10位），如：1418816972                        |
|      | key      | 是   | string | 在个人中心->我的数据,接口名称上方查看                 |

### 返回参数说明：



|      | 名称       | 类型   | 说明   |
| ---- | ---------- | ------ | ------ |
|      | error_code | int    | 返回码 |
|      | reason     | string |        |



### JSON返回示例

```
{
    "error_code": 0,
    "reason": "Success",
    "result": {
        "data": [
            {
                "content": "有一天晚上我俩一起吃西瓜，老大把西瓜籽很整洁的吐在了一张纸上，\r\n过了几天，我从教室回但宿舍看到老大在磕瓜子，\r\n我就问他：老大，你什么时候买的瓜子？\r\n老大说：刚晒好，说着抓了一把要递给我……",
                "hashId": "bcc5fdc2fb6efc6db33fa242474f108a",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "＂我女朋友气跑了＂\r\n＂怎么回事？严重吗？你怎么着她了？＂\r\n＂不严重，我只是很久没用了＂",
                "hashId": "03a6095c18e1d6fe7e2c19b2a20d03d1",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "还说神马来一场说走就走的旅行，\r\n工作后就连一场说走就走的下班都不行。",
                "hashId": "10edf75c1e7d0933c91f0f39a28a2c84",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "高速路上堵车，路边葡萄地里有一哥们竟然在偷葡萄，心想太没素质了吧！\r\n不管了我也去，刚溜进葡萄地，那哥们竟问我干嘛，\r\n我撇了一眼反问道你干嘛呢？\r\n那哥们答道摘葡萄呢！\r\n我答道：我也摘葡萄呢！\r\n哥们郁闷了说我摘我家的你呢？\r\n我顿时脸红，哥你家葡萄咋卖呢？",
                "hashId": "bb572bb5b4844badb31012983f7324f5",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "和老婆在街边散步，我手上捏着一张已揉成一团的传单，\r\n走了好一会终于看到个垃圾桶，我赶紧跑过去想扔掉，\r\n没想到老婆从后边一把拉住我说：老公，那个肯定吃不得了，别捡。\r\n我一愣，发现垃圾桶顶盖上放着半个西瓜。",
                "hashId": "7ebccd3bbfaf24e010f9eb3ee68234bd",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "某考生考了个倒数第一，回到家被他爸一顿暴揍，\r\n来到学校老师让他谈谈落后的体会，\r\n学生：“我终于明白了“落后就要挨打”的道理。”",
                "hashId": "4aee2aa6a79c67682f605c4146a8eca4",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "很多人不喜欢朝九晚五的生活，然后开始创业。\r\n最终，他的生活变成了朝五晚九。",
                "hashId": "7b358c4b96cf4a8d82b85545ea8f9603",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "钱这个东西，真是害人精。\r\n小到人与人之间的矛盾，大到国家之间的战争，无不是为了钱。\r\n钱可以把人推上万众瞩目之颠，也可以使人瞬间变成阶下囚。\r\n可是，富人们却没认识到，当钱几辈子花不完时，\r\n挣再多已经没有意义，还不如早日尽点社会责任，\r\n捐助给需要的人，求得个平安幸福。\r\n看到这个的有钱人们呐，你们什么时侯能捐我点啊！",
                "hashId": "94e18075f8c9c8211dfed5f8d6a62983",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "看到一句很好的名言：我们无法拉伸生命的长度，但是我们可以拓展生命的宽度。\r\n我觉得这句话太有道理了！\r\n意思就是：虽然我们无法再长高了，但是我们还可以继续长胖。",
                "hashId": "fd8e364a4c70d46e77c1610879748a9a",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            },
            {
                "content": "女生口中所说的“理工男好萌好棒好想嫁！”，\r\n其实理工男是指“会修电脑、会设置手机、会安家用电器、\r\n会帮做PPT打EXCEL表PS修图、话少、高冷、专一、不和乱七八糟的女生来往、\r\n不爱打扮却又干净清爽、高高瘦瘦、手指纤长、戴黑框眼镜超好看的帅哥”。\r\n其实找个帅哥让他学电脑，再戴个眼镜就OK了。",
                "hashId": "5001c08a3cc8a281b15c467bc15a4911",
                "unixtime": 1418814837,
                "updatetime": "2014-12-17 19:13:57"
            }
        ]
    }
}
```



接下来，我们开始今天的实战，首先创建一个项目NutJoke

![image-20220722080412586](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220722080412586.png)



点击下一步

![image-20220722080853387](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220722080853387.png)

因为我们要网络请求

所以我们需要在config.json中配置网络请求权限

## 网络请求的步骤

### 1、声明网络请求权限

在**entry**下的**config.json**中**module**字段下配置权限

```
"reqPermissions": [
   {
      "name": "ohos.permission.INTERNET"
   }
]
```

### 2、支持http明文请求

默认支持https，如果要支持http，在**entry**下的**config.json**中**deviceConfig**字段下配置

```
  "deviceConfig": {"default": {
    "network": {
      "cleartextTraffic": true
    }
  }},
```

### 3、创建HttpRequest

```
// 导入模块
import http from '@ohos.net.http';
// 创建HttpRequest对象
let httpRequest = http.createHttp();
```

### 4、发起请求

GET请求（**默认为GET请求**）

```
  // 请求方式：GET
  getRequest() {
    // 每一个httpRequest对应一个http请求任务，不可复用
    let httpRequest = http.createHttp()
    let url = 'https://devapi.qweather.com/v7/weather/now?location=101010100&key=48fbadf80bbc43ce853ab9a92408373e'
    httpRequest.request(url, (err, data) => {
      if (!err) {
        if (data.responseCode == 200) {
          console.info('=====data.result=====' + data.result)
          // 解析数据
          //this.content= data.result;
          // 解析数据
          var weatherModel: WeatherModel = JSON.parse(data.result.toString())
          // 判断接口返回码，0成功
          if (weatherModel.code == 200) {
            // 设置数据

            this.realtime = weatherModel.now


            this.isRequestSucceed = true;

            console.info('=====data.result===this.content==' + weatherModel.now)

          } else {
            // 接口异常，弹出提示
            prompt.showToast({ message: "数据请求失败" })
          }

        } else {
          // 请求失败，弹出提示
          prompt.showToast({ message: '网络异常' })
        }
      } else {
        // 请求失败，弹出提示
        prompt.showToast({ message: err.message })
      }
    })}
```



### 5、解析数据（简单示例）

1.网络请求到的json字符串

```js
/*
 * Copyright (c) 2021 JianGuo Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export function getTest() {
  return [
    {
      "content": "有一天晚上我俩一起吃西瓜，老大把西瓜籽很整洁的吐在了一张纸上，\r\n过了几天，我从教室回但宿舍看到老大在磕瓜子，\r\n我就问他：老大，你什么时候买的瓜子？\r\n老大说：刚晒好，说着抓了一把要递给我……",
      "hashId": "bcc5fdc2fb6efc6db33fa242474f108a",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "＂我女朋友气跑了＂\r\n＂怎么回事？严重吗？你怎么着她了？＂\r\n＂不严重，我只是很久没用了＂",
      "hashId": "03a6095c18e1d6fe7e2c19b2a20d03d1",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "还说神马来一场说走就走的旅行，\r\n工作后就连一场说走就走的下班都不行。",
      "hashId": "10edf75c1e7d0933c91f0f39a28a2c84",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "高速路上堵车，路边葡萄地里有一哥们竟然在偷葡萄，心想太没素质了吧！\r\n不管了我也去，刚溜进葡萄地，那哥们竟问我干嘛，\r\n我撇了一眼反问道你干嘛呢？\r\n那哥们答道摘葡萄呢！\r\n我答道：我也摘葡萄呢！\r\n哥们郁闷了说我摘我家的你呢？\r\n我顿时脸红，哥你家葡萄咋卖呢？",
      "hashId": "bb572bb5b4844badb31012983f7324f5",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "和老婆在街边散步，我手上捏着一张已揉成一团的传单，\r\n走了好一会终于看到个垃圾桶，我赶紧跑过去想扔掉，\r\n没想到老婆从后边一把拉住我说：老公，那个肯定吃不得了，别捡。\r\n我一愣，发现垃圾桶顶盖上放着半个西瓜。",
      "hashId": "7ebccd3bbfaf24e010f9eb3ee68234bd",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "某考生考了个倒数第一，回到家被他爸一顿暴揍，\r\n来到学校老师让他谈谈落后的体会，\r\n学生：“我终于明白了“落后就要挨打”的道理。”",
      "hashId": "4aee2aa6a79c67682f605c4146a8eca4",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "很多人不喜欢朝九晚五的生活，然后开始创业。\r\n最终，他的生活变成了朝五晚九。",
      "hashId": "7b358c4b96cf4a8d82b85545ea8f9603",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "钱这个东西，真是害人精。\r\n小到人与人之间的矛盾，大到国家之间的战争，无不是为了钱。\r\n钱可以把人推上万众瞩目之颠，也可以使人瞬间变成阶下囚。\r\n可是，富人们却没认识到，当钱几辈子花不完时，\r\n挣再多已经没有意义，还不如早日尽点社会责任，\r\n捐助给需要的人，求得个平安幸福。\r\n看到这个的有钱人们呐，你们什么时侯能捐我点啊！",
      "hashId": "94e18075f8c9c8211dfed5f8d6a62983",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "看到一句很好的名言：我们无法拉伸生命的长度，但是我们可以拓展生命的宽度。\r\n我觉得这句话太有道理了！\r\n意思就是：虽然我们无法再长高了，但是我们还可以继续长胖。",
      "hashId": "fd8e364a4c70d46e77c1610879748a9a",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    },
    {
      "content": "女生口中所说的“理工男好萌好棒好想嫁！”，\r\n其实理工男是指“会修电脑、会设置手机、会安家用电器、\r\n会帮做PPT打EXCEL表PS修图、话少、高冷、专一、不和乱七八糟的女生来往、\r\n不爱打扮却又干净清爽、高高瘦瘦、手指纤长、戴黑框眼镜超好看的帅哥”。\r\n其实找个帅哥让他学电脑，再戴个眼镜就OK了。",
      "hashId": "5001c08a3cc8a281b15c467bc15a4911",
      "unixtime": 1418814837,
      "updatetime": "2014-12-17 19:13:57"
    }
  ]
}
```

2.创建相应的对象

```js
export class JokeDetailData {
  content: string // 内容
  hashId: string // 哈希值
  unixtime: number //
  updatetime: string //更新时间


}
```



```
import { JokeDetailData } from './jokeDetailModel';

export class JokeModel {
  reason: string //返回说明
  error_code: number //返回码，0为查询成功

  result: {

    data: Array<JokeDetailData> // 笑话
  }
}


```



## 参考文档

- [聚合数据](https://www.juhe.cn/docs/api/id/95)

- [Text](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-text.md)
- [尺寸设置](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md)
- [边框设置](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-border.md)



## 项目地址

-https://gitee.com/jianguo888/nut-jokes